//
//  OBCountriesPickerView.swift
//  OBMusicBox
//
//  Created by Oleksandr Borovok on 1/17/18.
//  Copyright © 2018 Borovok. All rights reserved.
//

import UIKit

protocol CountriesPickerViewDelegate : UIPickerViewDelegate {
    func countryPicker(_ picker: CountriesPickerView, didSelectCountry country: Country)
}

class CountriesPickerView: UIPickerView {

    // MARK: - Properties Public
    var pickedCountry : Country?
    var countryDelegate : CountriesPickerViewDelegate?
    
    //
    private var countryData = [Country]()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.dataSource = self
        self.delegate = self
        loadData()
        setCurrentCountry()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate func loadData() {
        
        if let list = DataGateway.loadContries().0 {
        countryData = list
        countryData.sort { $1.name > $0.name }
        reloadAllComponents()
        }
    }
    
    func setCurrentCountry() {
        var countryCode: String?
        
        if let local = (Locale.current as NSLocale).object(forKey: NSLocale.Key.countryCode) as? String {
            countryCode = local
        }
        
        for country in countryData {
            if country.code == countryCode {
                pickedCountry = country
            }
        }
    }
}

extension CountriesPickerView : UIPickerViewDataSource {
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(countryData[row].emoji.description) - \(countryData[row].name.description)"
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countryData.count
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
}

extension CountriesPickerView : UIPickerViewDelegate {
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickedCountry = countryData[row]
        if let countryDelegate = self.countryDelegate {
            countryDelegate.countryPicker(self, didSelectCountry: countryData[row])
        }
    }
    
}
