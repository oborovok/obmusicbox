//
//  PlaylistModel.swift
//  OBMusicBox
//
//  Created by Oleksandr Borovok on 1/16/18.
//  Copyright © 2018 Borovok. All rights reserved.
//

import Foundation

// MARK: -
struct Playlist: Codable {
    
    enum CodingKeys: String, CodingKey {
        case topartists
    }
    
    let topartists: Toplists

}

// MARK: - Top list
struct Toplists: Codable {
    
    enum CodingKeys: String, CodingKey {
        case artist
        case attr = "@attr"
    }
    
    let artist: [Artist]
    let attr: ToplistsAttributes

}

struct ToplistsAttributes: Codable {
    
    enum CodingKeys: String, CodingKey {
        case country
        case page
        case perPage
        case totalPages
        case total
    }
    
    let country: String
    let page: String
    let perPage: String
    let totalPages: String
    let total: String
}

// MARK: - Artist
struct Artist: Codable {
    
    enum CodingKeys: String, CodingKey {
        case name
        case listeners
        case mbid
        case url
        case streamable
        case image
    }
    
    let name: String
    let listeners: String
    let mbid: String
    let url: String
    let streamable: String
    let image: [ArtistImage]
}

// MARK: - Image
enum ArtistImageSize: String {
    case small, medium, large, extralarge, mega
}

struct ArtistImage: Codable {
    
    enum CodingKeys: String, CodingKey {
        case text = "#text"
        case size
    }
    
    let text: String
    let size: String //ArtistImageSize

}
