//
//  OBCountry.swift
//  OBMusicBox
//
//  Created by Oleksandr Borovok on 1/17/18.
//  Copyright © 2018 Borovok. All rights reserved.
//

import UIKit

struct Country: Codable {
    
    enum CodingKeys: String, CodingKey {
        case name
        case code
        case emoji
        case unicode
        case title
    }

    public let name : String
    public let code : String
    public let emoji: String
    public let unicode : String
    public let title: String
}
