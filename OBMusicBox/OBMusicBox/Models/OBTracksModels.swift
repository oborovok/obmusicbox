//
//  OBTracksModels.swift
//  OBMusicBox
//
//  Created by Oleksandr Borovok on 1/18/18.
//  Copyright © 2018 Borovok. All rights reserved.
//

import Foundation

// MARK: -
struct ArtistTracksResult: Codable {
    
    enum CodingKeys: String, CodingKey {
        case artistTracks = "artisttracks"
    }
    
    let artistTracks: ArtistTracks
}

// MARK: - Artist Tracks
struct ArtistTracks: Codable {
    
    enum CodingKeys: String, CodingKey {
        case track
        case attr = "@attr"
    }
    
    let track: [Track]
    let attr: ArtistTracksAttributes
}

struct ArtistTracksAttributes: Codable {
    
    enum CodingKeys: String, CodingKey {
        case user
        case artist
        case page
        case perPage
        case totalPages
        case total
    }
    
    let user: String
    let artist: String
    let page: String
    let perPage: String
    let totalPages: String
    let total: String
}

// MARK: - Track
struct Track: Codable {
    
    enum CodingKeys: String, CodingKey {
        case artist
        case name
        case streamable
        case mbid
        case album
        case url
        case image
        case date
    }
    
    let artist: TrackArtistData
    let name: String
    let streamable: String
    let mbid: String
    let album: TrackAlbum
    let url: String
    let image: [ArtistImage]
    let date: TrackDate
}

struct TrackArtistData: Codable {
    enum CodingKeys: String, CodingKey {
        case text = "#text"
        case mbid
    }
    
    let text: String
    let mbid: String
}

struct TrackAlbum: Codable {
    
    enum CodingKeys: String, CodingKey {
        case text = "#text"
        case mbid
    }
    
    let text: String
    let mbid: String

}

struct TrackDate: Codable {
    
    enum CodingKeys: String, CodingKey {
        case text = "#text"
        case uts
    }
    
    let text: String
    let uts: String

}
