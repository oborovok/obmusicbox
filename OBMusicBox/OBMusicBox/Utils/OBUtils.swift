//
//  OBUtils.swift
//  OBMusicBox
//
//  Created by Oleksandr Borovok on 1/16/18.
//  Copyright © 2018 Borovok. All rights reserved.
//

import Foundation
import UIKit

struct OBReusableCellIdentifier {
    static let standart = "StandartCell"
}

struct OBColor {
    static let bordeaux = UIColor(red: 127 / 255, green: 12 / 255, blue: 1 / 255, alpha: 1)
    static let rose = UIColor(red: 255 / 255, green: 94 / 255, blue: 79 / 255, alpha: 1)
    static let vermilion = UIColor(red: 255 / 255, green: 24 / 255, blue: 2 / 255, alpha: 1)
    static let darkRed = UIColor(red: 127 / 255, green: 47 / 255, blue: 39 / 255, alpha: 1)
    static let lightRed = UIColor(red: 204 / 255, green: 19 / 255, blue: 2 / 255, alpha: 1)
    static let lightYellow = UIColor(red: 248 / 255, green: 241 / 255, blue: 218 / 255, alpha: 1)
}
