//
//  PlaylistViewController.swift
//  OBMusicBox
//
//  Created by Oleksandr Borovok on 1/16/18.
//  Copyright © 2018 Borovok. All rights reserved.
//

import UIKit

class PlaylistViewController: UIViewController {
    // MARK: - Properties Constants
    private let popArtistString = "Popular Artist"
    private let segueToSinger = "SegueFromMainToDetail"
    
    // MARK: - Properties Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties Public
    var playlistEntity: Playlist? {
        didSet {
            self.playlist = playlistEntity?.topartists.artist.sorted { $0.name < $1.name }
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Properties Private
    private var playlist: [Artist]?
    private var selectedArtist: Artist?
    private var refreshControl: UIRefreshControl!
    private var currentCountry: Country? {
        didSet {
            if let emoji = currentCountry?.emoji {
                self.topButton.setTitle(popArtistString + emoji, for: .normal)
                topButton.setTitleColor(OBColor.bordeaux, for: .normal)
            }
        }
    }
    private var countryPicker: CountriesPickerView?
    
    private let topButton = UIButton(frame: CGRect(x: 0, y: 0, width: 160, height: 40))
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationController()
        setupRefreshControl()
        
        countryPicker = CountriesPickerView(frame: CGRect(x: 0,y: 0,width: self.view.frame.size.width, height: 216))
        countryPicker?.backgroundColor = OBColor.lightYellow
        countryPicker?.isHidden = true
        currentCountry = countryPicker?.pickedCountry
        
        if let countryName = currentCountry?.name {
            getData(country: countryName)
        } else {
            getData(country: "israel")
        }
    }
    
    // MARK: - Navigation Controller
    func setupNavigationController() {
        let container = UIView(frame: CGRect(x: 0, y: 0, width: 160, height: 40))

        topButton.addTarget(self, action: #selector(PlaylistViewController.selectCountry), for: .touchUpInside)
        topButton.setTitle(popArtistString, for: .normal)
        topButton.setTitleColor(OBColor.bordeaux, for: .normal)

        container.addSubview(topButton)
        navigationItem.titleView = container
    }
    
    @objc func selectCountry() {
        countryPicker?.isHidden = false
        countryPicker?.countryDelegate = self
        
        if let picker = countryPicker {
            view.addSubview(picker)
        }
    }
    
    // MARK: - RefreshControl
    func setupRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(PlaylistViewController.refresh(sender:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @objc func refresh(sender:AnyObject) {
        getData(country: "israel")
    }
    
    // MARK: - get data
    func getData(country: String) {
        DataGateway.getPlaylist(country: country) { result in
            
            switch result {
            case .success(let playlist):
                self.playlistEntity = playlist
                self.refreshControl.endRefreshing()
            case .failure(let error):
                debugPrint(error.localizedDescription)
            }
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueToSinger {
            if let vc = segue.destination as? SingerViewController {
                vc.artist = selectedArtist
            }
        }
    }
}

extension PlaylistViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = playlist?.count {
            return count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return tableView.dequeueReusableCell(withIdentifier: OBReusableCellIdentifier.standart, for: indexPath)
    }
}

extension PlaylistViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let array = playlist {
            let artist = array[indexPath.row]
            cell.textLabel?.text = artist.name
            cell.detailTextLabel?.text = "( " + artist.listeners + " listeners )"
            let images = artist.image
            
            if let i = images.firstIndex(where: { $0.size == "large" }) {
                let element = images[i]
                
                DataGateway.loadImage(element.text, { (loadedImage, error) in
                    cell.imageView?.image = loadedImage
                })
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let array = playlist {
            selectedArtist = array[indexPath.row]
            performSegue(withIdentifier: segueToSinger, sender: self)
        }
    }
}

extension PlaylistViewController : CountriesPickerViewDelegate {
    func countryPicker(_ picker: CountriesPickerView, didSelectCountry country: Country) {
        getData(country: country.name)
        picker.isHidden = true
        currentCountry = country
    }
    
}
