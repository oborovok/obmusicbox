//
//  SingerViewController.swift
//  OBMusicBox
//
//  Created by Oleksandr Borovok on 1/16/18.
//  Copyright © 2018 Borovok. All rights reserved.
//

import UIKit

class SingerViewController: UIViewController {
    // MARK: - Properties Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties public
    public var artist: Artist? {
        didSet {
            let images = artist?.image
            
            if let i = images?.firstIndex(where: { $0.size == "extralarge" }) {
                let element = images?[i]
                
                if let text = element?.text {
                    DataGateway.loadImage(text, { (loadedImage, error) in
                        self.imageView.image = loadedImage
                    })
                }
            }
        }
    }
    
    // MARK: - Properties Private
    private var tracklist: [Track]? {
        didSet {
            tableView.reloadData()
        }
    }
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = artist?.name
        
        if let name = artist?.name {
            DataGateway.getTracks(forArtist: name) { (result) in
                
                switch result {
                case .success(let tracklist):
                    self.tracklist = tracklist.artistTracks.track
                case .failure(let error):
                    debugPrint(error.localizedDescription)
                }
            }
        }
    }
}

extension SingerViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = tracklist?.count {
            return count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: OBReusableCellIdentifier.standart, for: indexPath)
    }
}

extension SingerViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let list = tracklist {
            let track = list[indexPath.row]
            let rowNumber = indexPath.row + 1
            cell.textLabel?.text = String(rowNumber) + ". " + track.name
            cell.detailTextLabel?.text = "(" + track.date.text + ")"
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let list = tracklist {
            let track = list[indexPath.row]
            
            do {
               try UIApplication.shared.open(track.url.asURL(), options: [:], completionHandler: nil)
            } catch {
                debugPrint(error)
            }
        }
    }
}
