//
//  OBPlaylistApiRouter.swift
//  OBMusicBox
//
//  Created by Oleksandr Borovok on 1/16/18.
//  Copyright © 2018 Borovok. All rights reserved.
//

import Foundation
import Alamofire

public enum PlaylistApiRouter: URLRequestConvertible {

    case getPlaylist(String)
    
    var method : HTTPMethod {
        switch self {
        case .getPlaylist:
            return .get
        }
    }
    
    var path : String {
        switch self {
        case .getPlaylist:
            return "/2.0"
        }
    }
    
    var queryString : String {
        switch self {
        case .getPlaylist(let country):
            let path = "/?method=geo.gettopartists&country=" + country + "&api_key=" + NetworkSettings.kLastFmApiKey + "&format=json"
            
            if let url = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                return url
            }
            
            return path
        }
    }

    public func asURLRequest() throws -> URLRequest {

        let url = try (NetworkSettings.baseURL + path + queryString).asURL()
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.timeoutInterval = TimeInterval(10 * 1000)
        
        return try JSONEncoding.default.encode(request)
    }
    
}
