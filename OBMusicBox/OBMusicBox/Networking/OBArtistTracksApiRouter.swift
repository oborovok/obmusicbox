//
//  OBArtistTracksApiRouter.swift
//  OBMusicBox
//
//  Created by Oleksandr Borovok on 1/18/18.
//  Copyright © 2018 Borovok. All rights reserved.
//

import Foundation
import Alamofire

public enum ArtistTracksApiRouter: URLRequestConvertible {
    
    case getTracks(String)
    
    var method : HTTPMethod {
        switch self {
        case .getTracks:
            return .get
        }
    }
    
    var path : String {
        switch self {
        case .getTracks:
            return "/2.0"
        }
    }
    
    var queryString : String {
        switch self {
        case .getTracks(let artist):
            let path = "/?method=user.getartisttracks&user=rj&artist=" + artist + "&api_key=" + NetworkSettings.kLastFmApiKey + "&format=json"
            
            if let url = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                return url
            }
            
            return path
        }
    }
    
    public func asURLRequest() throws -> URLRequest {
        
        let url = try (NetworkSettings.baseURL + path + queryString).asURL()
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.timeoutInterval = TimeInterval(10 * 1000)
        
        return try JSONEncoding.default.encode(request)
    }
    
}
