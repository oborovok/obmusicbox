//
//  OBArtistTracksApiService.swift
//  OBMusicBox
//
//  Created by Oleksandr Borovok on 1/18/18.
//  Copyright © 2018 Borovok. All rights reserved.
//

import UIKit
import Alamofire

typealias tracksResult = (_ searchResult: Result<ArtistTracksResult>) -> ()

class ArtistTracksApiService: NSObject {

    func getTracks(forArtist artist: String, _ completion:@escaping tracksResult) {
        Alamofire.request(ArtistTracksApiRouter.getTracks(artist))
            .responseJSON { (response) in
                self.handler(response, completion)
        }
    }
}

//MARK: - Result handler
extension ArtistTracksApiService {
    func handler(_ response: DataResponse<Any>, _ completion:@escaping tracksResult) {
        if let error = response.result.error {
            completion(.failure(error))
            return
        }
        
        guard let data = response.data else {
            debugPrint("error trying to convert data to JSON")
            return
        }
        
        let decoder = JSONDecoder()
        let result = Result { try decoder.decode(ArtistTracksResult.self, from: data) }
        completion(result)
    }
}
