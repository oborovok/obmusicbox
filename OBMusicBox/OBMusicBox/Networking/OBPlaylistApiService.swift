//
//  OBPlaylistApiService.swift
//  OBMusicBox
//
//  Created by Oleksandr Borovok on 1/16/18.
//  Copyright © 2018 Borovok. All rights reserved.
//

import Foundation
import Alamofire

typealias playlistResult = (_ searchResult: Result<Playlist>) -> ()

class PlaylistApiService: NSObject {
    
    func getPlaylist(country: String, _ completion:@escaping playlistResult) {
        Alamofire.request(PlaylistApiRouter.getPlaylist(country))
            .responseJSON { (response) in
                self.handler(response, completion)
        }
    }

}

//MARK: - Result handler
extension PlaylistApiService {
    func handler(_ response: DataResponse<Any>, _ completion:@escaping playlistResult) {
        if let error = response.result.error {
            completion(.failure(error))
            return
        }
 
        guard let data = response.data else {
            debugPrint("error trying to convert data to JSON")
            return
        }
        
        let decoder = JSONDecoder()
        let result = Result { try decoder.decode(Playlist.self, from: data) }
        completion(result)
    }
}
