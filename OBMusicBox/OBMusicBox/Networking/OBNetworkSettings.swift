//
//  OBNetworkSettings.swift
//  OBMusicBox
//
//  Created by Oleksandr Borovok on 1/16/18.
//  Copyright © 2018 Borovok. All rights reserved.
//

import Foundation

struct NetworkSettings {
    
    static let baseURL = "http://ws.audioscrobbler.com"
    static let baseHost = "audioscrobbler.com"
    static let kLastFmApiKey = "e81f61890b7ff8633ca024d0faa449e7"

}
