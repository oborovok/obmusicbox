//
//  OBNetworkManager.swift
//  OBMusicBox
//
//  Created by Oleksandr Borovok on 1/17/18.
//  Copyright © 2018 Borovok. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

typealias imageResult = (_ image: UIImage?, _ error: Error?) -> ()

class NetworkManager: NSObject {
    //shared instance
    static let shared = NetworkManager()
    
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: NetworkSettings.baseHost)
    
    func startNetworkReachabilityObserver() {
        
        reachabilityManager?.listener = { status in
            switch status {
                
            case .notReachable:
                print("The network is not reachable")
                DataGateway.instance.isReachable = false
                
            case .unknown :
                print("It is unknown whether the network is reachable")
                DataGateway.instance.isReachable = true
                
            case .reachable(.ethernetOrWiFi):
                print("The network is reachable over the WiFi connection")
                DataGateway.instance.isReachable = true
                
            case .reachable(.wwan):
                print("The network is reachable over the WWAN connection")
                DataGateway.instance.isReachable = true
            }
        }
        
        // start listening
        reachabilityManager?.startListening()
    }
    
    class func loadImage(_ urlImage: String, _ completion:@escaping imageResult) {
        Alamofire.request(urlImage).responseImage { response in
            if let image = response.result.value {
                completion(image, nil)
            }
        }
    }
}
