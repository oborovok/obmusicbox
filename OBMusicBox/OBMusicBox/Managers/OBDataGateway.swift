//
//  OBDataGateway.swift
//  OBMusicBox
//
//  Created by Oleksandr Borovok on 1/16/18.
//  Copyright © 2018 Borovok. All rights reserved.
//

import UIKit

class DataGateway: NSObject {
    // MARK: - Singleton
    static let instance = DataGateway()

    private override init() {
        NetworkManager.shared.startNetworkReachabilityObserver()
    }
    
    // MARK: - Properties Public
    var isReachable = true
    
    // MARK: - Public
    class func getPlaylist(country: String, _ completion:@escaping playlistResult) {

        if DataGateway.instance.isReachable {
            PlaylistApiService().getPlaylist(country: country)  { (result) in
                completion(result)
                
//                OBFilesManager.instance.saveData(result, toFile: playlistFilename)
            }
        } else {
            FilesManager.instance.getPlist(completion)
        }
    }
    
    class func getTracks(forArtist artist: String, _ completion:@escaping tracksResult) {
        if DataGateway.instance.isReachable {
            ArtistTracksApiService().getTracks(forArtist: artist) { (result) in
                completion(result)
                
//                OBFilesManager.instance.saveData(result, toFile: artistTrackFileName)
            }
        } else {
            FilesManager.instance.getTracks(completion)
        }
    }
    
    class func loadImage(_ urlImage: String, _ completion:@escaping imageResult) {
        if DataGateway.instance.isReachable {
            NetworkManager.loadImage(urlImage, completion)
        }
    }
}

extension DataGateway {
    // MARK: - Countries from JSON
    class func loadContries() -> ([Country]?, Error?) {
        guard let path = Bundle.main.path(forResource: "EmojiCountryCodes", ofType: "json") else {
            print("could not find EmojiCountryCodes.json")
            return (nil, nil)
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            
            let decoder = JSONDecoder()
            let result = try decoder.decode([Country].self, from: data)
            return (result, nil)
        } catch {
            // handle error
            debugPrint("error trying to convert data to JSON")
            debugPrint(error)
            return (nil, error)
        }
    }
}
