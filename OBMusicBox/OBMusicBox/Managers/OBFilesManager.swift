//
//  OBFilesManager.swift
//  OBMusicBox
//
//  Created by Oleksandr Borovok on 1/22/18.
//  Copyright © 2018 Borovok. All rights reserved.
//

import UIKit

class FilesManager: NSObject {
    // MARK: - Properties Const
    private let defaultPlaylistFilename = "playlist"
    private let defaultArtistTrackFileName =  "ArtistTracks"
    private let cashPlaylistFilename = "cashplaylist.json"
    private let cashArtistTrackFileName =  "cashartisttracks.json"
    
    // MARK: - Singleton
    static let instance = FilesManager()
    
    private override init() { }
    
    // MARK: - Public
    public func getPlist(_ completion:@escaping playlistResult) {
        let _ = FilesManager.instance.getData(Playlist.self, fromFile: defaultPlaylistFilename)
    }
    
    public func getTracks(_ completion:@escaping tracksResult) {
        let _ = FilesManager.instance.getData(ArtistTracksResult.self, fromFile: defaultArtistTrackFileName)
    }
    
    public func savePlist(_ plist: Playlist) {
        saveData(plist, toFile: "plist.json")
    }
    
    func saveTracks(_ tracks: ArtistTracksResult) {
        saveData(tracks, toFile: "")
    }
    
    // MARK: - JSON from File (private)
    private func getData<T>(_ type: T.Type, fromFile named: String) -> Result<T, Error> where T :Decodable {
        do {
            guard let sourcefileURL = FilesManager.instance.getSourceFileUrl() else {
                return .failure(NSError(domain: "MusicDomain", code: 0, userInfo: [NSLocalizedDescriptionKey : "Wrong file"]))
            }
            
            let data = try Data(contentsOf: sourcefileURL, options: .mappedIfSafe)
            
            let decoder = JSONDecoder()
            let result = Result{ try decoder.decode(type, from: data)}
            return result
        } catch {
            // handle error
            debugPrint("error trying to convert data to JSON")
            debugPrint(error)
            return (.failure(error))
        }
    }
    
    private func getSourceFileUrl() -> URL? {

        guard let documentsDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            
            guard let path = Bundle.main.path(forResource: "named", ofType: "json") else {
                debugPrint("could not find EmojiCountryCodes.json")
                return nil
            }
            
            return URL(fileURLWithPath: path)
        }
        
        return documentsDirectoryUrl.appendingPathComponent("Persons.json")
    }
    
    // MARK: - JSON to File (private)
    private func saveData<T>(_ value: T, toFile named: String) where T : Encodable {
        //        let jsonFilePath = documentsDirectoryPath.URLByAppendingPathComponent("test.json")
        guard let path = Bundle.main.path(forResource: named, ofType: "json") else {
            debugPrint("could not find EmojiCountryCodes.json")
            return
        }
        
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(value)
            
            let file = try FileHandle(forWritingTo: path.asURL())
            file.write(data)
            print("JSON data was written to teh file successfully!")
        } catch let error as NSError {
            print("Couldn't write to file: \(error.localizedDescription)")
        }
    }
}
